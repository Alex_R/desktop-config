syntax on

set viminfo=""

set tabstop=4
set shiftwidth=4
set expandtab

colorscheme default