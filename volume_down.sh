#!/bin/bash

pactl list sinks | grep "Sink #" | sed 's/Sink #//g' | xargs -i{} pactl set-sink-volume {} -1000

VOLUME=$(pactl list sinks | grep "Sink #" -A 12 | grep Volume | head -n 1 | cut -d'/' -f 2 | awk '{$1=$1};1')
zenity --notification --timeout=1 --text="$VOLUME"
