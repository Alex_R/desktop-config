#!/bin/bash

xrandr --setprovideroutputsource modesetting NVIDIA-0

xrandr --output DP-4 --off --output HDMI-0 --off --output DP-2 --off --output DP-0 --off

xrandr --output DP-4   --auto --pos 1440x1440 --rate 179.86 --primary       \
       --output HDMI-0 --auto --pos 1440x0    --rate 59.95                  \
       --output DP-2   --auto --pos 0x320     --rate 74.97  --rotate right  \
       --output DP-0   --auto --pos 4000x320  --rate 74.97  --rotate left &

sleep 5

feh --bg-fill --no-xinerama /home/alex/.xmonad/magellan_cropped_bigk_scaled.png &
xsetroot -cursor_name left_ptr &
xrdb -merge .Xresources
setxkbmap gb
