import XMonad
import XMonad.Util.EZConfig
import XMonad.Hooks.EwmhDesktops
import XMonad.Layout.LayoutHints
import XMonad.Actions.Warp
import qualified XMonad.StackSet as W
import XMonad.Hooks.SetWMName
import XMonad.Hooks.EwmhDesktops

import System.Exit

main = do
    xmonad $ ewmh $ defaultConfig
        {
            terminal           = "xterm",
            borderWidth        = 1,
            focusedBorderColor = "#0000FF",
            startupHook        = spawn "/home/alex/.xmonad/desktop-init.sh",
            handleEventHook    = handleEventHook def <+> XMonad.Hooks.EwmhDesktops.fullscreenEventHook,
            layoutHook         = layoutHintsToCenter (Tall 1 (3/100) (1/2))
        } `additionalKeys` myKeys

myKeys =
    [
    ((mod1Mask, xK_a), warpToWindow (1/2) (1/2)),
    ((mod1Mask .|. shiftMask, xK_r), io(exitWith ExitSuccess)),
    ((mod1Mask, xK_r), spawn "xmonad --recompile && xmonad--restart;"),
    ((mod1Mask, xK_l), spawn "slock"),
    ((mod1Mask, xK_x), spawn "screenshot"),
    ((0, 0x1008ff2f), spawn "slock"), -- XF86Sleep
    ((0, 0x1008ff16), spawn "playerctl previous"), -- XF86AudioPrev
    ((0, 0x1008ff17), spawn "playerctl next"), -- XF86AudioNext
    ((0, 0x1008ff14), spawn "playerctl play-pause"), -- XF86AudioPlay
    ((0, 0x1008ff11), spawn "~/.xmonad/volume_down.sh"), -- XF86AudioLowerVolume
    ((0, 0x1008ff13), spawn "~/.xmonad/volume_up.sh"),  -- XF86AudioRaiseVolume
    ((0, 0x1008ff12), spawn "~/.xmonad/volume_mute.sh") -- XF86AudioMute
    ]
    ++
    [((m .|. mod1Mask, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_q, xK_w, xK_s, xK_e] [3,2,0,1]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]
