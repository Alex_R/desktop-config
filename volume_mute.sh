#!/bin/bash

pactl list sinks | grep "Sink #" | sed 's/Sink #//g' | xargs -i{} pactl set-sink-mute {} toggle
